`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:16:21 04/21/2014
// Design Name:   mux_deco
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_mux_deco.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mux_deco
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux_deco;

	// Inputs
	reg [15:11] instruccion_deco;
	reg [20:16] read_address2_deco;
	reg [4:0] reg31;
	reg [1:0] RegDst_ControlUnit;

	// Outputs
	wire [4:0] mux_deco;

	// Instantiate the Unit Under Test (UUT)
	mux_deco uut (
		.instruccion_deco(instruccion_deco), 
		.read_address2_deco(read_address2_deco), 
		.reg31(reg31), 
		.RegDst_ControlUnit(RegDst_ControlUnit), 
		.mux_deco(mux_deco)
	);

	initial begin
		// Initialize Inputs
		instruccion_deco = 0;
		read_address2_deco = 0;
		reg31 = 0;
		RegDst_ControlUnit = 0;

		// Wait 100 ns for global reset to finish
		#100;
      instruccion_deco = 5'b00110;
		read_address2_deco = 5'b00111;
		reg31 = 5'b11111;
		#50;
		RegDst_ControlUnit = 2'b00;
		#50;
		RegDst_ControlUnit = 2'b01;
		#50;
		RegDst_ControlUnit = 2'b10;
		#50;
		RegDst_ControlUnit = 2'b11;
		#50;
		RegDst_ControlUnit = 2'b00;
		
		
		
		// Add stimulus here

	end
      
endmodule

