`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:24:18 02/25/2014
// Design Name:   Fetch
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/tp_final_v1/test_fetch.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Fetch
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_fetch;

	// Inputs
	reg clk;
	reg [31:0] mux_jump_exe1;

	// Outputs
	wire [31:0] Fetch_Banco_registro;
	wire [31:0] Fetch_Sumador_fetch;

	// Instantiate the Unit Under Test (UUT)
	Fetch uut (
		.clk(clk), 
		.mux_jump_exe1(mux_jump_exe1), 
		.Fetch_Banco_registro(Fetch_Banco_registro), 
		.Fetch_Sumador_fetch(Fetch_Sumador_fetch)
	);
	always #50 clk=!clk;
	initial begin
		// Initialize Inputs
		clk = 0;
		mux_jump_exe1 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

