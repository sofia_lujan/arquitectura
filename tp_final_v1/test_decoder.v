`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//	Nombre del creador: Pablo Morales
// Create Date:   13:19:43 04/28/2014
// Design Name:   banco_registro_deco
// Module Name:   C:/Users/Pablo/Desktop/Arquitectura/New folder/tp_final_v1/test_decoder.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: banco_registro_deco
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_decoder;

	// Inputs
	reg [31:0] banco_registro_cod_Sync_IF_D_deco;
	reg [31:0] write_data;
	reg regWrite;
	reg clock;

	// Outputs
	wire [31:0] read_data1;
	wire [31:0] read_data2;

	// Instantiate the Unit Under Test (UUT)
	banco_registro_deco uut (
		.banco_registro_cod_Sync_IF_D_deco(banco_registro_cod_Sync_IF_D_deco), 
		.write_data(write_data), 
		.regWrite(regWrite), 
		.clock(clock), 
		.read_data1(read_data1), 
		.read_data2(read_data2)
	);

	initial begin
		// Initialize Inputs
		banco_registro_cod_Sync_IF_D_deco = 0;
		write_data = 0;
		regWrite = 0;
		clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
       // Initialize Inputs
		banco_registro_cod_Sync_IF_D_deco = 0;
		write_data = 0;
		regWrite = 0;
		clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
      banco_registro_cod_Sync_IF_D_deco=286226004;
		write_data = 1000;
		regWrite=0;
		
		#50;
      banco_registro_cod_Sync_IF_D_deco=186226004;
		write_data = 2000;
		regWrite=0;
		  
		#50;
      banco_registro_cod_Sync_IF_D_deco=126226004;
		write_data = 3000;
		regWrite=1; 
		// Add stimulus here

	end
      
endmodule

