`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:43:48 04/21/2014
// Design Name:   Sumador_fetch
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/tp_final_v1/test_Sumador_fetch.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Sumador_fetch
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_Sumador_fetch;

	// Inputs
	reg [31:0] PC_fetch;

	// Outputs
	wire [31:0] Sumador_fetch;

	// Instantiate the Unit Under Test (UUT)
	Sumador_fetch uut (
		.PC_fetch(PC_fetch), 
		.Sumador_fetch(Sumador_fetch)
	);
	
	initial begin
		// Initialize Inputs
		PC_fetch = 0;

		// Wait 100 ns for global reset to finish
		#10;
      PC_fetch = 1;
		#15
		PC_fetch = 4;
		
		// Add stimulus here

	end
      
endmodule

