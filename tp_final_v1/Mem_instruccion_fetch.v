`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:22:34 02/25/2014 
// Design Name: 
// Module Name:    Mem_instruccion_fetch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mem_instruccion_fetch(
	input [31:0] PC_fetch,
 	output reg [31:0] Banco_registro_cod
	
);


always @*
begin
case (PC_fetch)
32'h00000000: Banco_registro_cod = 32'b0;
32'h00000004: Banco_registro_cod = 32'b1;
32'h00000008: Banco_registro_cod = 32'b10;
32'h0000000C: Banco_registro_cod = 32'b11;
32'h00000010: Banco_registro_cod = 32'b100;
32'h00000014: Banco_registro_cod = 32'b101;
32'h00000018: Banco_registro_cod = 32'b110;
32'h0000001C: Banco_registro_cod = 32'b111;
default: Banco_registro_cod = 32'bz;
endcase
end

endmodule