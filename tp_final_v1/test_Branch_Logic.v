`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:40:23 04/22/2014
// Design Name:   Branch_Logic
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_Branch_Logic.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Branch_Logic
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_Branch_Logic;

	// Inputs
	reg Branch_ControlUnit;
	reg zero_alu_exe;

	// Outputs
	wire PC_BranchLogic;

	// Instantiate the Unit Under Test (UUT)
	Branch_Logic uut (
		.Branch_ControlUnit(Branch_ControlUnit), 
		.zero_alu_exe(zero_alu_exe), 
		.PC_BranchLogic(PC_BranchLogic)
	);

	initial begin
		// Initialize Inputs
		Branch_ControlUnit = 0;
		zero_alu_exe = 0;

		// Wait 100 ns for global reset to finish
		#100;
      Branch_ControlUnit = 0;
		zero_alu_exe = 0;
		#100;
      Branch_ControlUnit = 1;
		zero_alu_exe = 0; 
		#100;
      Branch_ControlUnit = 0;
		zero_alu_exe = 1; 
		#100;
      Branch_ControlUnit = 1;
		zero_alu_exe = 1; 
		// Add stimulus here

	end
      
endmodule

