`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:50:21 04/22/2014 
// Design Name: 
// Module Name:    shift_register_jump 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//					Toma los 26 bits menos significativos y los 
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module shift_register_jump(
	input [25:0] instruccion, 
	output reg [27:0] shift_register_jump
    );

always @*
begin
shift_register_jump = instruccion <<< 2; 
end
endmodule
