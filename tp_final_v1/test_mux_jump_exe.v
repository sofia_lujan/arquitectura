`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:27:47 04/22/2014
// Design Name:   mux_jump_exe
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_mux_jump_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mux_jump_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux_jump_exe;

	// Inputs
	reg [1:0] jump_ControlUnit;
	reg [31:0] mux_branch_exe;
	reg [31:0] shift_register_jump;
	reg [31:0] ALU_result_exe;

	// Outputs
	wire [31:0] mux_jump_exe;

	// Instantiate the Unit Under Test (UUT)
	mux_jump_exe uut (
		.jump_ControlUnit(jump_ControlUnit), 
		.mux_branch_exe(mux_branch_exe), 
		.shift_register_jump(shift_register_jump), 
		.ALU_result_exe(ALU_result_exe), 
		.mux_jump_exe(mux_jump_exe)
	);

	initial begin
		// Initialize Inputs
		jump_ControlUnit = 0;
		mux_branch_exe = 0;
		shift_register_jump = 0;
		ALU_result_exe = 0;

		// Wait 100 ns for global reset to finish

		#50;
		jump_ControlUnit = 1;
      mux_branch_exe = 1;
		shift_register_jump = 2;
		ALU_result_exe = 3;
		#50;
		jump_ControlUnit = 2;
      mux_branch_exe = 1;
		shift_register_jump = 2;
		ALU_result_exe = 3;
		#50;
		jump_ControlUnit = 3;
      mux_branch_exe = 1;
		shift_register_jump = 2;
		ALU_result_exe = 3;
		#50;
		jump_ControlUnit = 0;
      mux_branch_exe = 1;
		shift_register_jump = 2;
		ALU_result_exe = 3;
		// Add stimulus here

	end
      
endmodule

