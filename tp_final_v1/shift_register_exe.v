`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:06:46 04/21/2014 
// Design Name: 
// Module Name:    shift_register_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module shift_register_exe(
    input [31:0] extended_instruction_deco,
    output reg [31:0] shift_register_exe
    );

always @*
begin
shift_register_exe = extended_instruction_deco <<< 2;  //calculamos la pr�xima posici�n de memoria a leer
end
endmodule
