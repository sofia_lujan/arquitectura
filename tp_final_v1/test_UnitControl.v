`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:07:05 04/28/2014
// Design Name:   ControlUnit
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_UnitControl.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ControlUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_UnitControl;

	// Inputs
	reg [31:0] intruccion_fin_deco;

	// Outputs
	wire [1:0] jump_ControlUnit;
	wire [1:0] MemtoReg_ControlUnit;
	wire [1:0] RegDst_ControlUnit;
	wire [2:0] ALUOp_ControlUnit;
	wire ALUSrc_ControlUnit;
	wire Branch_ControlUnit;
	wire MemRead_ControlUnit;
	wire MemWrite_ControlUnit;
	wire RegWrite_ControlUnit;

	// Instantiate the Unit Under Test (UUT)
	ControlUnit uut (
		.jump_ControlUnit(jump_ControlUnit), 
		.MemtoReg_ControlUnit(MemtoReg_ControlUnit), 
		.RegDst_ControlUnit(RegDst_ControlUnit), 
		.ALUOp_ControlUnit(ALUOp_ControlUnit), 
		.ALUSrc_ControlUnit(ALUSrc_ControlUnit), 
		.Branch_ControlUnit(Branch_ControlUnit), 
		.intruccion_fin_deco(intruccion_fin_deco), 
		.MemRead_ControlUnit(MemRead_ControlUnit), 
		.MemWrite_ControlUnit(MemWrite_ControlUnit), 
		.RegWrite_ControlUnit(RegWrite_ControlUnit)
	);

	initial begin
		// Initialize Inputs
		intruccion_fin_deco = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

