`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:40:43 06/09/2014
// Design Name:   Sync_D_EX
// Module Name:   C:/Users/Pablo/Documents/arquitectura/tp_final_v1/Test_Sync_Exec.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Sync_D_EX
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Test_Sync_Exec;

	// Inputs
	reg clk;
	reg [31:0] read_data1_input;
	reg [31:0] read_data2_input;
	reg [31:0] extended_instruction_deco_input;
	reg [27:0] shift_register_jump_input;
	reg [1:0] jump_ControlUnit_input;
	reg [1:0] MemtoReg_ControlUnit_input;
	reg [2:0] ALUOp_ControlUnit_input;
	reg ALUSrc_ControlUnit_input;
	reg Branch_ControlUnit_input;
	reg MemRead_ControlUnit_input;
	reg MemWrite_ControlUnit_input;
	reg [31:0] Fetch_Sumador_fetch_input;

	// Outputs
	wire [31:0] read_data1_output;
	wire [31:0] read_data2_output;
	wire [31:0] extended_instruction_deco_output;
	wire [31:0] shift_register_jump_output;
	wire [1:0] jump_ControlUnit_output;
	wire [1:0] MemtoReg_ControlUnit_output;
	wire [2:0] ALUOp_ControlUnit_output;
	wire ALUSrc_ControlUnit_output;
	wire Branch_ControlUnit_output;
	wire MemRead_ControlUnit_output;
	wire MemWrite_ControlUnit_output;
	wire [31:0] Fetch_Sumador_fetch_output;

	// Instantiate the Unit Under Test (UUT)
	Sync_D_EX uut (
		.clk(clk), 
		.read_data1_input(read_data1_input), 
		.read_data2_input(read_data2_input), 
		.extended_instruction_deco_input(extended_instruction_deco_input), 
		.shift_register_jump_input(shift_register_jump_input), 
		.jump_ControlUnit_input(jump_ControlUnit_input), 
		.MemtoReg_ControlUnit_input(MemtoReg_ControlUnit_input), 
		.ALUOp_ControlUnit_input(ALUOp_ControlUnit_input), 
		.ALUSrc_ControlUnit_input(ALUSrc_ControlUnit_input), 
		.Branch_ControlUnit_input(Branch_ControlUnit_input), 
		.MemRead_ControlUnit_input(MemRead_ControlUnit_input), 
		.MemWrite_ControlUnit_input(MemWrite_ControlUnit_input), 
		.Fetch_Sumador_fetch_input(Fetch_Sumador_fetch_input), 
		.read_data1_output(read_data1_output), 
		.read_data2_output(read_data2_output), 
		.extended_instruction_deco_output(extended_instruction_deco_output), 
		.shift_register_jump_output(shift_register_jump_output), 
		.jump_ControlUnit_output(jump_ControlUnit_output), 
		.MemtoReg_ControlUnit_output(MemtoReg_ControlUnit_output), 
		.ALUOp_ControlUnit_output(ALUOp_ControlUnit_output), 
		.ALUSrc_ControlUnit_output(ALUSrc_ControlUnit_output), 
		.Branch_ControlUnit_output(Branch_ControlUnit_output), 
		.MemRead_ControlUnit_output(MemRead_ControlUnit_output), 
		.MemWrite_ControlUnit_output(MemWrite_ControlUnit_output), 
		.Fetch_Sumador_fetch_output(Fetch_Sumador_fetch_output)
	);
	always #50 clk=!clk;
	initial begin
		// Initialize Inputs
		clk=0;
		read_data1_input = 0;
		read_data2_input = 0;
		extended_instruction_deco_input = 0;
		shift_register_jump_input = 0;
		jump_ControlUnit_input = 0;
		MemtoReg_ControlUnit_input = 0;
		ALUOp_ControlUnit_input = 0;
		ALUSrc_ControlUnit_input = 0;
		Branch_ControlUnit_input = 0;
		MemRead_ControlUnit_input = 0;
		MemWrite_ControlUnit_input = 0;
		Fetch_Sumador_fetch_input = 0;
		
		
			#100// Wait 100 ns for global reset to finish
		
		
		read_data1_input = 1900000000;
		read_data2_input = 1000230000;
		extended_instruction_deco_input = 10000000;
		shift_register_jump_input = 100000;
		jump_ControlUnit_input = 3; 
		MemtoReg_ControlUnit_input = 2;
		ALUOp_ControlUnit_input = 4;
		ALUSrc_ControlUnit_input = 1;
		Branch_ControlUnit_input = 1;
		MemRead_ControlUnit_input = 1;
		MemWrite_ControlUnit_input = 1;
		Fetch_Sumador_fetch_input = 1200000000;

	
        
		
		
		// Add stimulus here

	end
      
endmodule

