`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:23:27 02/25/2014 
// Design Name: 
// Module Name:    Fetch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Fetch(
	input clk,
	input [31:0] mux_jump_exe1, //este valor es el que viene del m�dulo Exe (se lo pasamos al m�dulo PC)
	output [31:0]Fetch_Banco_registro, //este valor es el que viene del bloque de sincronizacion
	output [31:0]Fetch_Sumador_fetch   //este valor es el que viene del bloque de sincronizacion
    );

wire [31:0]PC_wire; //este cable conecta el bloque PC con el bloque Instruction Memory
wire [31:0]Inst_mem_wire; //este cable coneta el bloque Mem_instruccion con el bloque Sync_IF_D
wire [31:0]Sumador_wire; //este cable coneta el bloque Sumador_fetch con el bloque 


// Instantiate the module
PC_fetch instance_PC_fetch (
    .mux_jump_exe(mux_jump_exe1), //toma el valor de la entrada al bloque Fetch (mux_jump_exe toma el valor de mux_jump_exe1 que es la entrada al m�dulo top)
    .clk(clk), 
    .PC_fetch(PC_wire) //esta salida entra al m�dulo al bloque Sumador y Mem_instruction
    );
	 
// Instantiate the module
Sumador_fetch instance_Sumador_fetch (
    .PC_fetch(PC_wire),  //este valor viene del bloque PC_fetch
    .Sumador_fetch(Sumador_wire) //lo que sale de aca se conecta con la entrada del bloque de sincronizci�n
    );

// Instantiate the module
/*Mem_instruccion_fetch instance_name (
    .PC_fetch(PC_wire),   //este valor viene del bloque PC_fetch
	 .Banco_registro_cod(Inst_mem_wire) //lo que sale de aca se conecta con la entrada del bloque de sincronizci�n
    );*/

// Instantiate the module
InstructionMemory instance_InstructionMemory (
    .clka(clk),  //clock de sincronizacion
    .addra(PC_wire),  //este valor viene del bloque PC_fetch
    .douta(Inst_mem_wire) //este valor sale de la la memoria
    );

// Instantiate the module
Sync_IF_D instance_Sync_IF_D (
    .Banco_registro_cod(Inst_mem_wire), //este valor viene del bloque Mem_instruccion_fetch
    .Sumador_fetch(Sumador_wire), //este valor viene del bloque Sumador_fetch
    .clk(clk), 
    .Banco_registro_cod_Sync_IF_D(Fetch_Banco_registro), //este valor pasa a ser la salida del bloque Fetch
    .Sumador_fetch_Sync_IF_D(Fetch_Sumador_fetch)    //este valor pasa a ser la salida del bloque Fetch
    );

endmodule

