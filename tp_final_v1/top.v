`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:02:15 06/10/2014 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
	input clk
    );

wire [31:0] sumador_fetch_wire; //este cable conecta el la salida del latch del fetch con la entrada a la decodificacion
wire [31:0] registro_fetch_wire;
wire [31:0] mux_memoria_wire; //une el mux de la etapa write back con el file register
wire [31:0] read_data_1_wire; //une el register file con la alu
wire [31:0] read_data_2_wire; //une el register file con la alu
wire [31:0] sign_extend_wire; //
wire [31:0] shiftToMuxJumpwire; //conecta el shift left 2 con el mux_jump_exe
wire [31:0] sumador_fetch_deco_exe_wire; //este valor conecta el valor que viene del fetch, pasa por deco y llega a exe
wire [1:0]	jump_control_wire; //sale de la etapa de decodificación y entra a la ejecución
wire [1:0]	memToReg_wire; //cable que une el deco con la exe
wire [2:0]	AluOP_deco_Exe_wire;
wire AluSrc_wire; //une deco con exe
wire branch_wire; //une deco con exe
wire [4:0]  amountShift_wire; //une deco con exe
wire [5:0]	instrAluControl_wire; //este valor será la entrada a la alu control
wire [31:0] muxJump_wire; //este cable une el mux jump de la etapa de ejecución con el pc del fetch
wire [31:0] alu_sync_wire; //este valor sale de la etapa de ejecucion y entra a la etapa de memoria
wire [31:0] write_data_wire; //une el register file con el data memory
wire [1:0] memReg_in_wire; //es de la alu control, comunica la ejecucion con la memoria
wire [31:0] address_in_wire; //une el valor que viene de la alu de la etapa exe con la entrada 0 del mux de la ultima etapa
wire [1:0] memReg_mem_wire; //valor que viene desde el control unit hasta el mux de la ultima etapa
wire [31:0] read_data_wire; //une el data memory con el mux de la ultima etapa
wire [31:0] sumador_fetch_Sync_Exe_wire; //este valor une la etapa exe con la memoria
wire [31:0] sumador_fetch_Mem_Sync_wire; //este cable une la etapa de memoria con el write back
// Instantiate the module
Fetch instance_Fetch (
    .clk(clk), 
    .mux_jump_exe1(muxJump_wire), //este valor viene del mux del jump de la etapa exe
    .Fetch_Banco_registro(registro_fetch_wire), //este valor entra a la etapa de ejecucion
    .Fetch_Sumador_fetch(sumador_fetch_wire) //este valor entra a la etapa de ejecucion
    );


// Instantiate the module
Deco instance_Deco (
    .clk(clk), 
    .Fetch_Banco_registro_input(registro_fetch_wire), //este valor viene del instruction memory de la etapa fecth
    .Fetch_Sumador_fetch_input(sumador_fetch_wire), //este valor viene del sumador de la etapa fecth
    .write_data_input(mux_memoria_wire),  //viene del mux de la etapa write back
    .reg31_input(reg31_input), 
    .Deco_RegisterFile_read_data1_output(read_data_1_wire),  //va a la alu de la etapa exe
    .Deco_RegisterFile_read_data2_output(read_data_2_wire),  //va a la alu de la etapa exe
    .Deco_SignExten_extended_instruction_output(sign_extend_wire), //va al mux exe y al shift  
    .Deco_shiftLeft_register_jump_output(shiftToMuxJumpwire), //va al mux_jump_exe
    .Deco_Sumador_fetch_input(sumador_fetch_deco_exe_wire),  //este valor entra a la etapa exe
    .Deco_jump_ControlUnit_output(jump_control_wire), //se une con la entrada de la ejecución
    .Deco_MemtoReg_ControlUnit_output(memToReg_wire), //sale del deco y entra a la exe
    .Deco_ALUOp_ControlUnit_output(AluOP_deco_Exe_wire), //sale del deco y entra a la exe
    .Deco_ALUSrc_ControlUnit_output(AluSrc_wire), //va a la etapa de ejecución
    .Deco_Branch_ControlUnit_output(branch_wire), //va a la etapa de ejecución
    .Deco_MemRead_ControlUnit_output(Deco_MemRead_ControlUnit_output), 
    .Deco_MemWrite_ControlUnit_output(Deco_MemWrite_ControlUnit_output), 
    .Deco_shift_amount_output(amountShift_wire), //va a la etapa de ejecución
    .Deco_a_AluControl_output(instrAluControl_wire) //va a la etapa de ejecución
    );


// Instantiate the module
Exe instance_Exe (
    .clk(clk), 
    .read_data_1_deco_input(read_data_1_wire),  //viendel register file
    .extended_instruction_deco_input(sign_extend_wire), //viene del sign extend 
    .read_data_2_deco_input(read_data_2_wire),  //viene del register file
    .ALUSrc_ControlUnit_input(AluSrc_wire), //viene de la etapa de decodificación
    .branch_input(branch_wire), //viene de la decodificación
    .Sumador_fetch_input(sumador_fetch_deco_exe_wire), //Este valor viene de la etapa de decodificación
    .jump_ControlUnit_input(jump_control_wire), //viene de la etapa de decodificación
    .ALUOp_controlUnit_input(AluOP_deco_Exe_wire), //une deco con exe
    .instruccion_deco_input(instrAluControl_wire),  //viene de la deco
    .shift_register_deco_input(shiftToMuxJumpwire), //viene del shift left de la etapa deco
    .shift_amount_input(amountShift_wire), //viene de la decodificación
	 .memToReg(memToReg_wire), //viene de la decodificación
    .mux_jump_exe_Sync_Exe(muxJump_wire), //este valor entra a la etapa fetch
    .ALU_result_exe_Sync_Exe(alu_sync_wire), //este valor conecta la alu de la ejecucion con la etapa de memoria
    .mux_branch_exe_i_Sync_Exe(mux_branch_exe_i_Sync_Exe), 
	 .memReg_Sync_Exe(memReg_in_wire), //este valor se lo pasa a la memoria
	 .data2_output_Sync_Exe(write_data_wire), //este cable pasa el dato que viene del register file a la memoria
	 .sumador_fetch_Sync_Exe(sumador_fetch_Sync_Exe_wire) //Este valor va a la etapa de la memoria para ser usada en la etapa sig
    );
	 
	 

// Instantiate the module
Memoria instance_Memoria (
    .clk(clk), 
    .WRE(WRE), 
    .address(alu_sync_wire), //viene de la alu de la etapa anterior
    .write_data(write_data_wire), //este cable pasa el dato que viene del register file a la memoria
	 .memReg_in(memReg_in_wire), //viene de la ejecucion
	 .sumador_fetch(sumador_fetch_Sync_Exe_wire), //viene de la etapa anterior
    .read_data(read_data_wire), //va al multiplexor de la etapa siguiente
	 .memReg_output(memReg_mem_wire), //va al selector del mux de la ultima etapa
	 .address_in(address_in_wire), //es el valor que viene de la alu de la etapa de exe y va a la ultima etapa
	 .sumador_fetch_Mem_Sync(sumador_fetch_Mem_Sync_wire) //Este valor entra al multiplexor de la etapa siguiente
    );


// Instantiate the module
mux_memoria instance_mux_memoria (
    .Memtoreg_ControlUnit(memReg_mem_wire),  //valor de control unit que viene de la etapa anterior
    .ALU_result_exe(address_in_wire), //este valor viene de la etapa de ejecucion
    .mux_memoria(mux_memoria_wire), //este valor entra al registerFile
    .Sumador_fetch(sumador_fetch_Mem_Sync_wire), //viene de la etapa anterior
    .DataMemory_Memoria(read_data_wire) //valor que viene del data memory de la etapa anterior
    );
	


endmodule
