`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:25:00 05/27/2014
// Design Name:   Exe
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_exe;

	// Inputs
	reg clk;
	reg [31:0] read_data_1_deco_input;
	reg [31:0] extended_instruction_deco_input;
	reg [31:0] read_data_2_deco_input;
	reg ALUSrc_ControlUnit_input;
	reg branch_input;
	reg [31:0] Sumador_fetch_input;
	reg [1:0] jump_ControlUnit_input;
//	reg [3:0] ALU_Control_exe_input;
   reg [2:0] ALUOp_controlUnit_input;//este valor entra a la alu_exe
   reg [5:0] instruccion_deco_input;  //este valor entra a la alu_exe
	reg [31:0] shift_register_deco_input;
	reg [4:0] shift_amount_input;

	// Outputs
	wire [31:0] mux_jump_exe_Sync_Exe;
	wire [31:0] ALU_result_exe_Sync_Exe;
	wire [31:0] mux_branch_exe_i_Sync_Exe; //ESTE ES SOLO DE PRUEBA

	// Instantiate the Unit Under Test (UUT)
	Exe uut (
		.clk(clk), 
		.read_data_1_deco_input(read_data_1_deco_input), 
		.extended_instruction_deco_input(extended_instruction_deco_input), 
		.read_data_2_deco_input(read_data_2_deco_input), 
		.ALUSrc_ControlUnit_input(ALUSrc_ControlUnit_input), 
		.branch_input(branch_input), 
		.Sumador_fetch_input(Sumador_fetch_input), 
		.jump_ControlUnit_input(jump_ControlUnit_input), 
		//.ALU_Control_exe_input(ALU_Control_exe_input), 
		.ALUOp_controlUnit_input(ALUOp_controlUnit_input),
		.instruccion_deco_input(instruccion_deco_input),
		.shift_register_deco_input(shift_register_deco_input), 
		.shift_amount_input(shift_amount_input),
		.mux_jump_exe_Sync_Exe(mux_jump_exe_Sync_Exe), 
		.ALU_result_exe_Sync_Exe(ALU_result_exe_Sync_Exe),
		.mux_branch_exe_i_Sync_Exe //ESTE ES SOLO DE PRUEBA
	);
	
	
	always #50 clk=!clk;
	initial begin
		// Initialize Inputs
		clk = 0;
		read_data_1_deco_input = 0;//
		extended_instruction_deco_input = 0;//
		read_data_2_deco_input = 0;//
		ALUSrc_ControlUnit_input = 0;//
		branch_input = 0;//
		Sumador_fetch_input = 0;//
		jump_ControlUnit_input = 0;//
		ALUOp_controlUnit_input = 0;//
		instruccion_deco_input = 0;//
		shift_register_deco_input = 0;//
		shift_amount_input = 0;//

		// Wait 100 ns for global reset to finish
		#100;
      read_data_1_deco_input = 32'h00000005;  
		read_data_2_deco_input = 32'h00000002;
		extended_instruction_deco_input = 32'h00000004;
		ALUSrc_ControlUnit_input = 0;
		ALUOp_controlUnit_input = 0;
		instruccion_deco_input = 0;
		branch_input = 0;
		Sumador_fetch_input = 32'h00000009;
		jump_ControlUnit_input = 2'b00;
		shift_register_deco_input = 32'h00000007;
		
		#100;
      read_data_1_deco_input = 32'h00000005; //register_file (no importa)
		read_data_2_deco_input = 32'h00000002; //register_file
		extended_instruction_deco_input = 32'h00000004; //del sign_extend (no importa)
		ALUSrc_ControlUnit_input = 0; //control_unit, hace que el mux tome el valor del read_data2
		ALUOp_controlUnit_input = 3'b111; //alu_control
		instruccion_deco_input = 6'b000100; //mando 0 para hacer un SLL, alu_control
		branch_input = 0; //control_unit (no importa)
		Sumador_fetch_input = 32'h00000009; //(no importa)
		jump_ControlUnit_input = 2'b00;  //(no importa)
		shift_register_deco_input = 32'h00000007;		//(no importa)
		shift_amount_input = 5'b00010; //cantidad de bits que se va a desplazar
		
		// Add stimulus here

	end
      
endmodule

