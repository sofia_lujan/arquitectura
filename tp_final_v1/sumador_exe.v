`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:23:18 04/21/2014 
// Design Name: 
// Module Name:    sumador_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sumador_exe(
    input [31:0] shift_register_exe,
    input [31:0] Sumador_fetch,
    output reg[31:0] sumador_exe
    );
	 
always @*
begin
sumador_exe = Sumador_fetch + shift_register_exe;  
end

endmodule


