`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:02:11 05/19/2014 
// Design Name: 
// Module Name:    Exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Exe(
	input clk,
	input [31:0] read_data_1_deco_input, //viene del Register File
	input [31:0] extended_instruction_deco_input, //viene de la extensión de signo
	input [31:0] read_data_2_deco_input, //viene del Register File 
	input ALUSrc_ControlUnit_input, //viene del Control_Unit
	input branch_input, //viene del ControlUnit
	input [31:0] Sumador_fetch_input, //este valor viene del sumador de la fetch
	input [1:0] jump_ControlUnit_input, //este valor viene del controlUnit
//	input [3:0]  ALU_Control_exe_input, //este valor es la entrada a la alu
   input [2:0] ALUOp_controlUnit_input, //este valor entra a la alu_control
   input [5:0] instruccion_deco_input,  //este valor entra a la alu_control
	input [31:0] shift_register_deco_input, //este valor conecta el shift register y el mux_jump_exe
	input [4:0] shift_amount_input, //este valor entra a la alu_exe
	input [1:0] memToReg, //es un flag del control unit, pasa directamente a la salida del módulo
	output [31:0] mux_jump_exe_Sync_Exe, //salida del mux_jump_exe
	output [31:0] ALU_result_exe_Sync_Exe, //salida de la alu
	output [31:0] mux_branch_exe_i_Sync_Exe, //ESTE ES SOLO DE PRUEBA
	output [1:0] memReg_Sync_Exe, //este valor es el mismo que el de entrada, sale sin ser procesado
	output [31:0] data2_output_Sync_Exe, //va a la etapa de memoria
	output [31:0] sumador_fetch_Sync_Exe //este valor sale de la etapa
    );

wire [31:0] mux_exe_wire;
wire [31:0] sumador_exe_wire;
wire PC_BranchLogic_wire; 
//wire [31:0] Sumador_fetch_wire; //este valor viene del sumador de la etapa fetch y entrad al mux_branch_exe 
wire [31:0] mux_branch_exe_wire; //este valor conecta los dos multiplexores (branch y jump)
wire [31:0] shift_register_exe_wire; //este valor es una de las entradas del sumador_exe
wire [31:0] ALU_result_exe_wire; //este cable conecta la alu y la etapa de sincronizacion
wire [31:0] mux_jump_exe_wire; 
wire zero_alu_exe_wire; //este valor conecta la alu con la and
wire [3:0]  ALU_Control_exe_wire; //este valor es la entrada a la alu

// Instantiate the module
alu_exe instance_alu_exe (
	 .shift_amount(shift_amount_input), //este valor viene a la entrada de la etapa
    .ALU_Control_exe(ALU_Control_exe_wire), //Este valor es la salida de alu-control
    .mux_exe(mux_exe_wire),  //esta entrada es la salida del mux_exe
    .read_data_1_deco(read_data_1_deco_input), //esta es la salida del Register File (sincronización)
    .ALU_result_exe(ALU_result_exe_wire), //esta salida entra a la etapa de sincronizacion
    .zero_alu_exe(zero_alu_exe_wire) //este valor se multiplica con el flag branch para ser la entrada PC_BranchLogic de mux_branch_exe
    );

// Instantiate the module
add_exe instance_add_exe (
    .branch_flag(branch_input), //este valor es una entrada a la etapa
    .zero_flag(zero_alu_exe_wire), //este valor viene de la alu_exe
    .add_exe_output(PC_BranchLogic_wire)
    );
	 
// Instantiate the module
mux_exe instance_mux_exe (
    .ALUSrc_ControlUnit(ALUSrc_ControlUnit_input), //este valor viene del Control_Unit
    .extended_instruction_deco(extended_instruction_deco_input),  //este valor viene de la extensión de signo (sincronización)
    .mux_exe(mux_exe_wire),  //esta salida es la entrada a la ALU
    .read_data_2_deco(read_data_2_deco_input) //esta entrada viene de lal Register File (sincronización)
    );
	 
// Instantiate the module
mux_branch_exe instance_mux_branch_exe (
    .PC_BranchLogic(PC_BranchLogic_wire), //este valor viene de la operación and entre el brach del control_unit y el flag zero de alu_exe
    .sumador_exe(sumador_exe_wire), //este valor viene del sumador_exe
    .Sumador_fetch(Sumador_fetch_input),  //este valor viene del sumador de la etapa fetch
    .mux_branch_exe(mux_branch_exe_wire) //este valor es una de las entradas del mux_jump_exe
    );
	 
// Instantiate the module
mux_jump_exe instance_mux_jump_exe (
    .jump_ControlUnit(jump_ControlUnit_input), //este valor viene del control unit
    .mux_branch_exe(mux_branch_exe_wire), //este viene del mux_branch_exe
    .shift_register_jump(shift_register_deco_input), //este valor viene del shift_register
    .ALU_result_exe(Sumador_fetch_input), //este valor viene del sumador de la etapa del fetch
    .mux_jump_exe(mux_jump_exe_wire) //ete valor va a ser la entrada de la sincronizacion
    );
	 
// Instantiate the module
shift_register_exe instance_shift_register_exe (
    .extended_instruction_deco(extended_instruction_deco_input), //este valor es la entrada al shift_register_exe
    .shift_register_exe(shift_register_exe_wire) //este valor es la entrada del jump_mux_exe
    );
	 
// Instantiate the module
sumador_exe instance_sumador_exe (
    .shift_register_exe(shift_register_exe_wire),  //este valor viene del shift_left de la etapa anterior
    .Sumador_fetch(Sumador_fetch_input), //este valor viene del sumador del fetch
    .sumador_exe(sumador_exe_wire) //este valor va a una entrada del mux_branch_exe
    );
	 
// Instantiate the module
Sync_Exe instance_Sync_Exe (
    .clk(clk), 
    .mux_jump_exe(mux_jump_exe_wire), //este valor es la salida del multiplexor del jump
    .ALU_result_exe(ALU_result_exe_wire), //Este valor viene de la alu
	 .mux_branch_exe_i(mux_branch_exe_wire), //ESTE ES SOLO DE PRUEBA
	 .memReg(memToReg), //viene de la entrada al modulo exe
	 .data2(read_data_2_deco_input), //viene del register file
	 .sumador_fetch(Sumador_fetch_input), //este valor viene de la entrada
    .mux_jump_exe_Sync_Exe(mux_jump_exe_Sync_Exe),  //se conectan a la salida del modulo
    .ALU_result_exe_Sync_Exe(ALU_result_exe_Sync_Exe),  //se conectan a la salida del modulo
	 .mux_branch_exe_i_Sync_Exe(mux_branch_exe_i_Sync_Exe),
	 .memReg_Sync_Exe(memReg_Sync_Exe),
	 .data2_output_Sync_Exe(data2_output_Sync_Exe),
	 .sumador_fetch_Sync_Exe(sumador_fetch_Sync_Exe) //este valor es salida del modulo de sincronizacion
    );
	 

// Instantiate the module
ALU_Control instance_ALU_Control (
    .ALU_Control_exe(ALU_Control_exe_wire),  //este valor entra a la alu_exe
    .ALUOp_controlUnit(ALUOp_controlUnit_input), //este valor viene de la etapa deco
    .instruccion_deco(instruccion_deco_input) //este valor viene de la etapa deco
    );
	 
endmodule
