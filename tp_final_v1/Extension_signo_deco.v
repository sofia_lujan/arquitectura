`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:52:39 02/26/2014 
// Design Name: 
// Module Name:    Extension_signo_deco 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Extension_signo_deco(
	input [15:0] instruccion_deco,
	output reg [31:0] extended_instruction_deco
    );

always @*
begin
extended_instruction_deco = instruccion_deco;
if (instruccion_deco[15]==1)
 extended_instruction_deco[31] = 1;
end

endmodule
