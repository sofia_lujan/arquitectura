`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:56:11 04/22/2014 
// Design Name: 
// Module Name:    mux_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_exe(
    input ALUSrc_ControlUnit,
    input [31:0] extended_instruction_deco,
    output reg [31:0] mux_exe,
    input [31:0] read_data_2_deco
    );

always @*
begin
if (ALUSrc_ControlUnit)
begin
mux_exe = extended_instruction_deco;	
end
else
begin
mux_exe = read_data_2_deco;
end
 
end
endmodule

