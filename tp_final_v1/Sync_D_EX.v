`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:31:56 06/08/2014 
// Design Name: 
// Module Name:    Sync_D_EX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sync_D_EX(
	input clk,
	input [31:0] read_data1_input, //salidas del banco de registro (register file) que entra al lach
   input [31:0] read_data2_input,	 //salidas del banco de registro (register file) que entra al lach
	input [31:0] extended_instruction_deco_input, //salida con signo extendido del modulo SignExtend que entra al lach
	input [27:0] shift_register_jump_input,	//salida del shift register << 2 que entra al lach

	input [1:0] jump_ControlUnit_input,
   input [1:0] MemtoReg_ControlUnit_input,
      
   input [2:0] ALUOp_ControlUnit_input,
   input  ALUSrc_ControlUnit_input,
   input  Branch_ControlUnit_input,
   input  MemRead_ControlUnit_input,
   input  MemWrite_ControlUnit_input,
   
	input [31:0] Fetch_Sumador_fetch_input,
	input [31:0] Fetch_Banco_registro_input, //entrada de los 32 bit de las intrucciones que se le pasa a la alu control
															// y a el shift amount
	
	output reg [31:0] read_data1_output, //salidas del banco de registro (register file) que entra al lach
   output reg [31:0] read_data2_output,	 //salidas del banco de registro (register file) que entra al lach
	output reg [31:0] extended_instruction_deco_output, //salida con signo extendido del modulo SignExtend que entra al lach
	output reg [31:0] shift_register_jump_output,	//salida del shift register << 2 que entra al lach

	output reg [1:0] jump_ControlUnit_output,
   output reg [1:0] MemtoReg_ControlUnit_output,
   output reg [2:0] ALUOp_ControlUnit_output,
   output reg ALUSrc_ControlUnit_output,
   output reg Branch_ControlUnit_output,
   output reg MemRead_ControlUnit_output,
   output reg MemWrite_ControlUnit_output,
	
	output reg [31:0] Fetch_Sumador_fetch_output,
	output reg [4:0] shift_amount_output,
	output reg [5:0] a_AluControl_output
	
	
);
//reg [31:0] shift_register_jump_output1;
 
always @ (posedge clk)
begin

read_data1_output = read_data1_input;
read_data2_output = read_data2_input;
extended_instruction_deco_output = extended_instruction_deco_input;
shift_register_jump_output = shift_register_jump_input;
Fetch_Sumador_fetch_output = Fetch_Sumador_fetch_input;
jump_ControlUnit_output = jump_ControlUnit_input;
MemtoReg_ControlUnit_output = MemtoReg_ControlUnit_input;
ALUOp_ControlUnit_output =  ALUOp_ControlUnit_input;
ALUSrc_ControlUnit_output = ALUSrc_ControlUnit_input;
Branch_ControlUnit_output = Branch_ControlUnit_input;
MemRead_ControlUnit_output = MemRead_ControlUnit_input;
MemWrite_ControlUnit_output = MemWrite_ControlUnit_input;

shift_register_jump_output = {Fetch_Sumador_fetch_input[31:28],shift_register_jump_input};

shift_amount_output = Fetch_Banco_registro_input[10:6];
a_AluControl_output = Fetch_Banco_registro_input[5:0];
end

endmodule
