`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:16:23 06/10/2014 
// Design Name: 
// Module Name:    Sync_Mem_WB 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sync_Mem_WB(
	 input clk,
	 input [31:0] read_data_input,
	 input [1:0]  memToReg,
	 input [31:0] address_in,
	 input [31:0] sum_fetch,
	 output reg [31:0] read_data_output,
	 output reg [1:0]  memToReg_output,
	 output reg [31:0] address_out_Sync,
	 output reg [31:0] sum_fetch_Sync
	 
    );
	 
always @ (posedge clk)
begin
read_data_output = read_data_input;
memToReg_output = memToReg; 
address_out_Sync = address_in;
sum_fetch_Sync = sum_fetch; 
end	 


endmodule
