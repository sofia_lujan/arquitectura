`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//	Nombre del creador: Pablo Morales
// Create Date:   13:19:43 04/28/2014
// Design Name:   banco_registro_deco
// Module Name:   C:/Users/Pablo/Desktop/Arquitectura/New folder/tp_final_v1/test_decoder.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: banco_registro_deco
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:10:36 04/28/2014 
// Design Name: 	Pablo Morales el unico

// Module Name:    banco_registro_deco 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module banco_registro_deco(
	 input [31:0] banco_registro_cod_Sync_IF_D_deco, //entrada
    input [31:0] write_data,
	 input [4:0] write_addr,//ver esto que cambie a ver si funca
    input regWrite,
	 input clock,
    output [31:0] read_data1,
    output [31:0] read_data2	 

    );
	 
//Lo primero que debo hacer es asignar la entrada que es de 32 bit a todos
//estos registros que toman solo una parte de la entrada general
//Como hago la asignacion?
reg [4:0] read_addr1; // 4 bit de entrada ( cuando lo instancio solo le paso los 4 bits que quiero pasarle)
reg [4:0] read_addr2;
//reg [4:0] write_addr;//Write addr es la cantidad de entradas que tiene el registro!





reg [31:0] registro [31:0];
reg [31:0] read_1_data=0;//aca voy a guardar los valores que despues van a ir al output read_data1 y lo inicializo en 0
reg [31:0] read_2_data=0;//aca voy a guardar los valores que despues van a ir al output read_data2 y lo inicializo en 0

initial
begin
	registro[0]=100000;
	registro[1]=2600000;
	registro[2]=300000;
	registro[3]=400000;
	registro[5]=500000;
	registro[6]=600000;
	registro[7]=700000;
	registro[8]=800000;
	registro[8]=900000;
	registro[10]=1000000;
	registro[11]=1100000;
	registro[12]=1200000;
	registro[13]=1300000;
	registro[14]=1400000;
	registro[16]=1500000;
	registro[17]=1600000;
	registro[18]=1700000;
	registro[19]=1800000;
	registro[20]=1900000;
	registro[21]=2000000;
	registro[22]=2100000;
	registro[23]=2200000;
	registro[24]=2300000;
	registro[25]=2400000;
	registro[26]=2500000;
	registro[27]=2600000;
	registro[28]=2700000;
	registro[29]=2800000;
	registro[30]=2900000;
	registro[31]=3000000;
	
end
//hago las asignaciones correspondientes para que me queden separados en las cantidades de bit que quiero.
	 
always @(posedge clock) //cada vez que viene el clock
	 begin
		read_addr1=banco_registro_cod_Sync_IF_D_deco[25:21];
	   read_addr2=banco_registro_cod_Sync_IF_D_deco[20:16];
	   //write_addr=banco_registro_cod_Sync_IF_D_deco[15:11];
			 
		if(regWrite)//si la bandera es 1;
		begin
			registro[write_addr] = write_data;//En el registro numero "write_address" se guarda la write data.
																// si el Reg_write es =1
		end
		read_1_data = registro[read_addr1];//Paso el valor del registro (en la posicion correspondiente) al dato de registro
		read_2_data = registro[read_addr2];//paso el valor del registro (en la posicion correspondiente) al dato de registro de salida
		
	end

//ahora lo unico que hago es uno con un wire.

assign read_data1 = read_1_data;
assign read_data2 = read_2_data;
endmodule
