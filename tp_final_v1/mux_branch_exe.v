`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:23:07 04/22/2014 
// Design Name: 
// Module Name:    mux_branch_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_branch_exe(
	input PC_BranchLogic, //este bien es el que hace de selector para saber cu�l va a ser la pr�x direcci�n a ejecutarse
	input [31:0] sumador_exe, //este valor viene del sumador de la etapa de ejecuci�n
	input [31:0] Sumador_fetch, //este es el valor que viene de la primer etapa, trae la pr�x posici�n de memoria
	output reg [31:0] mux_branch_exe //esta salida entra al mux del jump
    );

always @*
begin
case(PC_BranchLogic)
1'b0:  mux_branch_exe = Sumador_fetch; //sale el par�metro que viene del sumador de la etapa de fetch
1'b1: mux_branch_exe = sumador_exe; //sale el par�metro que viene del sumador de la etapa de ejecuci�n
endcase
end

endmodule
