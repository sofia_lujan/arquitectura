`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:38:38 04/22/2014 
// Design Name: 
// Module Name:    Branch_Logic 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Branch_Logic(
    input Branch_ControlUnit,
    input zero_alu_exe,
    output reg PC_BranchLogic
    );
always @*
begin
PC_BranchLogic = zero_alu_exe | Branch_ControlUnit;  
end

endmodule
