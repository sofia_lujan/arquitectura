`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:57:37 05/26/2014 
// Design Name: 
// Module Name:    Deco 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Deco(
	
	//inputs
	input clk,
	//Banco de registro
	input [31:0] Fetch_Banco_registro_input, //este valor es el que viene del bloque de sincronizacion
	input [31:0] Fetch_Sumador_fetch_input,   //este valor es el que viene del bloque de sincronizacion
	input [31:0] write_data_input, //Esta se�al viene del ultumo mux al finalizar todo el pipline.(etapa write-back)
	input [4:0] reg31_input, //registro extra que va a entrar al mux y su valor esta guardado en alguna parte $31
	
	
	
	//outputs tienen que ser salidas del bloque de sincronizacion
	output [31:0] Deco_RegisterFile_read_data1_output, //salidas del banco de registro (register file)
   output [31:0] Deco_RegisterFile_read_data2_output,	 //salidas del banco de registro (register file)
	output [31:0] Deco_SignExten_extended_instruction_output, //salida con signo extendido del modulo SignExtend
	output [31:0] Deco_shiftLeft_register_jump_output,	//salida del shift register << 2
	
	output [31:0] Deco_Sumador_fetch_input, //Salida del modulo que viene del SYNC, es el valor de ADD
	
	output [1:0] Deco_jump_ControlUnit_output,//Salida del modulo que viene del SYNC, es la se�al Jump del ControlUnit
   output [1:0] Deco_MemtoReg_ControlUnit_output,//Salida del modulo que viene del SYNC, es la se�al MemToReg del ControlUnit
   output [2:0] Deco_ALUOp_ControlUnit_output,//Salida del modulo que viene del SYNC, es la se�al AluOp del ControlUnit
   output  Deco_ALUSrc_ControlUnit_output,//
   output  Deco_Branch_ControlUnit_output,
   output  Deco_MemRead_ControlUnit_output,
   output  Deco_MemWrite_ControlUnit_output,
	
	output  [4:0] Deco_shift_amount_output,
	output  [5:0] Deco_a_AluControl_output
	
    );
	 
	
	
	//wire para llegar entre modulos
	
	 //registers wires
	 wire [4:0] mux_deco_output_input; // Salida del mux y entrada al Register file (rite addr)
	 wire [31:0] read_data1_output_input; //salidas del banco de registro (register file)
	 wire [31:0] read_data2_output_input;	 //salidas del banco de registro (register file)
	 wire [31:0] extended_instruction_deco_output_input; //salida del sing extend
	 wire [27:0] shift_register_jump_output_input;	//salida del shift register << 2

	 //sign extension wires
	 //wire [31:0] sign_extended_value;
	 //wire [31:0] sign_extended_value_shifted;

	 //branch
	 //wire [10:0] branch_dest_addr_aux;

	 //Control Signals Input
	 wire [1:0] jump_ControlUnit_output_input; //sale del Control unit y entra al SYNC
	 wire [1:0] MemtoReg_ControlUnit_output_input;//sale del Contol uni y entra al sync
    wire [2:0] ALUOp_ControlUnit_output_input;//Sale del control unit y entra al sync
    wire  ALUSrc_ControlUnit_output_input;//sale del control unit y entra al sync
    wire  Branch_ControlUnit_output_input;//sale del control unit y entra al sync
    wire  MemRead_ControlUnit_output_input;//mem read control unit sale del mismo queu nombre anteriormente (control unit) y entre al sync
    wire  MemWrite_ControlUnit_output_input;
    wire [1:0] RegDst_ControlUnit_output_input;
	 wire RegWrite_ControlUnit_output_input;
	
	//wire RegDst_in;
	 //wire ALUSrc_in;
	 //wire MemToReg_in;
	 //wire MemRead_in;
	 //wire MemWrite_in;
	 //wire Branch_in;
	 //wire [1:0] ALUOp_in;
	 //wire [2:0] trunk_mode_in;
	 //wire Bne_in;
	 //wire Jump_in;
	 //wire [1:0]Jdes_sel;
	 
	 
// Instanciacion del Banco de registro
banco_registro_deco banco_de_registro (
    .banco_registro_cod_Sync_IF_D_deco(Fetch_Banco_registro_input),//aca va la instruccion de 32 bits que entra, adentro del
																						 //modulo se separa en distintas partes
    .write_data(write_data_input),//Esta se�al viene de la ultima etapa
    .regWrite(RegWrite_ControlUnit_output_input),// Viene del Control Unit el RegWrite_ControlUnit_output_input como un wire
    .clock(clk), //el clock
    .read_data1(read_data1_output_input), //conecto directamente las salidas a la salida de la instancia de la etapa.
    .read_data2(read_data2_output_input),	//conecto directamente las salidas a la salida de la instancia de la etapa.
	 .write_addr(mux_deco_output_input) // este cable comunica la salida del mux con la entrada del banco de registros
    );

// Instanciacion del decodificador de tres entradas
mux_deco mux_3_entradas (
    .instruccion_deco(Fetch_Banco_registro_input [15:11]),//Fetch_Banco_registro_input [15:11] es la entrada al mux -> instruccion_deco
    .read_address2_deco(Fetch_Banco_registro_input [20:16]), //Fetch_Banco_registro_input [20:16] es la entrada read_address2_deco 
    .reg31(reg31_input [4:0]), 
    .RegDst_ControlUnit(RegDst_ControlUnit_output_input),//conexion (wire) que viene del ControlUnit RegDst
    .mux_deco(mux_deco_output_input)//es la salida del mux, son 4 bit, osea (direcciona 32 registros)
    );
// Instanciacion del control unit
ControlUnit control_unit (
    .jump_ControlUnit(jump_ControlUnit_output_input),//salida del Jump del control unit que va conectada al SYNC 
    .MemtoReg_ControlUnit(MemtoReg_ControlUnit_output_input), //salida del MemToReg del control unit que va conectada al SYNC
    .RegDst_ControlUnit(RegDst_ControlUnit_output_input), //se�al de control que entra al mux_3_entradas
    .ALUOp_ControlUnit(ALUOp_ControlUnit_output_input), //se�al de control ALU OP del control unit que va conectada al SYNC
    .ALUSrc_ControlUnit(ALUSrc_ControlUnit_output_input), //se�al que sale del Control unit y va al SYNC
    .Branch_ControlUnit(Branch_ControlUnit_output_input),//se�al que sale del control unit y va al sync 
    .intruccion_fin_deco(Fetch_Banco_registro_input), //entra la se�al que son los 32 bit de intrucciones
    .MemRead_ControlUnit(MemRead_ControlUnit_output_input),//se�al que sale del control unit y va al sync  
    .MemWrite_ControlUnit(MemWrite_ControlUnit_output_input),//se�al que sale del control unit y va al sync 
    .RegWrite_ControlUnit(RegWrite_ControlUnit_output_input)//El valor este sale y entra al Register File.(banco de registros)
    );

// Instanciacion del sign extend
Extension_signo_deco sign_extend (
    .instruccion_deco(Fetch_Banco_registro_input [15:0]), //Entrada de los 15 bit mas bajo de la instruccion.
    .extended_instruction_deco(extended_instruction_deco_output_input) //salida de los 31 bit cuando se hace la extension de signo
    );


// Instanciacion shift register jump

shift_register_jump shift_left2 (
    .instruccion(Fetch_Banco_registro_input [25:0]), //25 bit de la instruccion de entrada, se le hace un shift de 2.
    .shift_register_jump(shift_register_jump_output_input) //Salida del shif register 28 bit
    );
// Instantiate the module
Sync_D_EX Sync_Deco_Exec (
    .clk(clk), 
    .read_data1_input(read_data1_output_input), //salidas del Register file que entran al sync
    .read_data2_input(read_data2_output_input), //salidas del register file que entran al sync
    .extended_instruction_deco_input(extended_instruction_deco_output_input), //salida del sing extend q entra al sync
    .shift_register_jump_input(shift_register_jump_output_input),// salida del shift register q entra al lach
    .jump_ControlUnit_input(jump_ControlUnit_output_input),//entrada que viene del Control UNIT - JUMP
    .MemtoReg_ControlUnit_input(MemtoReg_ControlUnit_output_input),//entrada que viene del Control Unit - MemToReg
    .ALUOp_ControlUnit_input(ALUOp_ControlUnit_output_input), //entrada que viene del control UNIT - ALUop
    .ALUSrc_ControlUnit_input(ALUSrc_ControlUnit_output_input),//entrada quev iene del control unit - ALUsrc 
    .Branch_ControlUnit_input(Branch_ControlUnit_output_input),//entrada que viene del control unit - branch control 
    .MemRead_ControlUnit_input(MemRead_ControlUnit_output_input), //entrada que viene del control unit- memread
    .MemWrite_ControlUnit_input(MemWrite_ControlUnit_output_input), //entrada que viene del control unit memewrite
	 
	 .Fetch_Sumador_fetch_input(Fetch_Sumador_fetch_input),//entrada del sumador que entra al modulo
	 
    .read_data1_output(Deco_RegisterFile_read_data1_output), //Esta salida va como salida del modulo
    .read_data2_output(Deco_RegisterFile_read_data2_output), //Esta salida va como salida del modulo
    .extended_instruction_deco_output(Deco_SignExten_extended_instruction_output), // Esta salida va como salida del modulo
    .shift_register_jump_output(Deco_shiftLeft_register_jump_output), // Esta salida va como salida del modulo
    .jump_ControlUnit_output(Deco_jump_ControlUnit_output), //Esta salida va como salida del modulo
    .MemtoReg_ControlUnit_output(Deco_MemtoReg_ControlUnit_output), //Esta salida va como salida del modulo
    .ALUOp_ControlUnit_output(Deco_ALUOp_ControlUnit_output), //Esta salida va como salida del modulo
    .ALUSrc_ControlUnit_output(Deco_ALUSrc_ControlUnit_output), //Esta salida va como salida del modulo
    .Branch_ControlUnit_output(Deco_Branch_ControlUnit_output), //Esta salida va como salida del modulo
    .MemRead_ControlUnit_output(Deco_MemRead_ControlUnit_output),//Esta salida va como salida del modulo
    .MemWrite_ControlUnit_output(Deco_MemWrite_ControlUnit_output), //Esta salida va como salida del modulo
	 
	 .Fetch_Sumador_fetch_output(Deco_Sumador_fetch_input), //la salida del sumador q entra al SYNC sale como salida del modulo
    .Fetch_Banco_registro_input(Fetch_Banco_registro_input), //la salida de las intrucciones q entra al sync
	 
	 .shift_amount_output(Deco_shift_amount_output),
	 .a_AluControl_output(Deco_a_AluControl_output)
	 
	 );











endmodule
