`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:48:17 04/28/2014
// Design Name:   mux_memoria
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_mux_memoria.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mux_memoria
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux_memoria;

	// Inputs
	reg [1:0] Memtoreg_ControlUnit;
	reg [31:0] ALU_result_exe;
	reg [31:0] Sumador_fetch;
	reg [31:0] DataMemory_Memoria;

	// Outputs
	wire [31:0] mux_memoria;

	// Instantiate the Unit Under Test (UUT)
	mux_memoria uut (
		.Memtoreg_ControlUnit(Memtoreg_ControlUnit), 
		.ALU_result_exe(ALU_result_exe), 
		.mux_memoria(mux_memoria), 
		.Sumador_fetch(Sumador_fetch), 
		.DataMemory_Memoria(DataMemory_Memoria)
	);

	initial begin
		// Initialize Inputs
		Memtoreg_ControlUnit = 0;
		ALU_result_exe = 0;
		Sumador_fetch = 0;
		DataMemory_Memoria = 0;

		// Wait 100 ns for global reset to finish
		#100;
		Memtoreg_ControlUnit = 0;
		ALU_result_exe = 1;
		Sumador_fetch = 2;
		DataMemory_Memoria = 3;
		#150
		Memtoreg_ControlUnit = 1;
		ALU_result_exe = 1;
		Sumador_fetch = 2;
		DataMemory_Memoria = 3;
		#150
		Memtoreg_ControlUnit = 3;
		ALU_result_exe = 1;
		Sumador_fetch = 2;
		DataMemory_Memoria = 3;
		#150
		Memtoreg_ControlUnit = 4;
		ALU_result_exe = 1;
		Sumador_fetch = 2;
		DataMemory_Memoria = 3;
		// Add stimulus here

	end
      
endmodule

