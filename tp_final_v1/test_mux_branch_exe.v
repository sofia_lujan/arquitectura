`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:35:30 04/22/2014
// Design Name:   mux_branch_exe
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_mux_branch_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mux_branch_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux_branch_exe;

	// Inputs
	reg PC_BranchLogic;
	reg [31:0] sumador_exe;
	reg [31:0] Sumador_fetch;

	// Outputs
	wire [31:0] mux_branch_exe;

	// Instantiate the Unit Under Test (UUT)
	mux_branch_exe uut (
		.PC_BranchLogic(PC_BranchLogic), 
		.sumador_exe(sumador_exe), 
		.Sumador_fetch(Sumador_fetch), 
		.mux_branch_exe(mux_branch_exe)
	);

	initial begin
		// Initialize Inputs
		PC_BranchLogic = 0;
		sumador_exe = 0;
		Sumador_fetch = 0;

		// Wait 100 ns for global reset to finish
		#100;
		sumador_exe = 32'h00001111;
		Sumador_fetch = 32'h11110000;
		
		#20;
		PC_BranchLogic = 1;
		
		#20;
		PC_BranchLogic = 0;
		// Add stimulus here

	end
      
endmodule

