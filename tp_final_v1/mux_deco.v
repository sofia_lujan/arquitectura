`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:18:20 02/26/2014 
// Design Name: 
// Module Name:    mux_deco 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_deco(
	input [15:11] instruccion_deco, //son 5 bits que son leidos de la instruccion
	input [20:16] read_address2_deco, //son 5 bits que son leidos de la instruccion
	input [4:0] reg31,  //estos 5 bits corresponden al registro 31 usado para las instrucciones de jump
	input [1:0] RegDst_ControlUnit, //con estos dos bits seleccionamos la entrada deseada
	output reg [4:0] mux_deco //esta salida va a ser la entrada al register file (a la entrada write address)
    );
//assign reg31 = 5'b11111; // la posici�n de memoria del reg31 es 11111, ya que es el �ltimo registro disponible de los 32

always @ (RegDst_ControlUnit)
begin
case (RegDst_ControlUnit)
2'b00: mux_deco = read_address2_deco; //la direcci�n que escribimos en el register file esta dada por los bits 20:16 de la instrucci�n
2'b01: mux_deco = instruccion_deco; //la direcci�n que escribimos en el register file esta dada por los bits 15:11 de la instrucci�n
2'b10: mux_deco = reg31; //la direcci�n que escribimos en el register file esta dada por el registro 31
default: mux_deco = read_address2_deco; //FALTA ANALIZAR SI ESTA ES LA SALIDA CONVENIENTE POR DEFECTO
endcase
end 

endmodule
