`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:51:07 04/22/2014 
// Design Name: 
// Module Name:    ALU_Control 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ALU_Control(
    output reg [3:0] ALU_Control_exe,
    input [2:0] ALUOp_controlUnit,
    input [5:0] instruccion_deco
    );
/*
* El control Unit procesa el opcode de la instruccion (verifica si es R-type, I-type o J-type)
* y obtenes el ALUOp que es un valor de 3 bits. En caso del R-type y tengo en cuenta el campo function que es del 0 al 6 de la intruccion
*
*/
always @*
begin 
case (ALUOp_controlUnit)
3'b000: ALU_Control_exe = 4'b0010; //ADDI - ADDIU - LUI - LB-LH-LW-LWU-LBU-LHU-SB-SH-SW
3'b001: ALU_Control_exe = 4'b0000; //ANDI
3'b010: ALU_Control_exe = 4'b0001; //ORI
3'b011: ALU_Control_exe = 4'b0011;  //XORI
3'b100: ALU_Control_exe = 4'b0110; //BEQ - BNE
3'b101: ALU_Control_exe = 4'b0001;
3'b110: ALU_Control_exe = 4'b0111; //SLTI - SLTIU
3'b111: // JR - JALR - SLL-SRL-SRA-SLLV-SRLV-SRAV-ADD-ADDU-SUB-SUBU-AND-OR-XOR-NOR-SLT-SLTU

	begin
		case (instruccion_deco)
		6'b100000: ALU_Control_exe =4'b0010; //ADD
		6'b100010: ALU_Control_exe =4'b0110; //SUB
		6'b100100: ALU_Control_exe =4'b0000; //AND
		6'b100101: ALU_Control_exe =4'b0001; //OR
		6'b101010: ALU_Control_exe =4'b0111; //SLT
		6'b001000: ALU_Control_exe =4'b0010; //ADD
		6'b001001: ALU_Control_exe =4'b0010; //ADD
		6'b000000: ALU_Control_exe =4'b1000; //SLL
		6'b000010: ALU_Control_exe =4'b0100; //SRL
		6'b000011: ALU_Control_exe =4'b0101; //SRA
		6'b000100: ALU_Control_exe =4'b1000; //SLL
		6'b000110: ALU_Control_exe =4'b0100; //SRL
		6'b000111: ALU_Control_exe =4'b0101; //SRA
		6'b100001: ALU_Control_exe =4'b1010; //ADDU
		6'b100011: ALU_Control_exe =4'b1011; //SUBU
		6'b100110: ALU_Control_exe =4'b0011; //XOR
		6'b100111: ALU_Control_exe =4'b1001; //NOR
		6'b101011: ALU_Control_exe =4'b0111; //SLT
		default: ALU_Control_exe = 4'bZZZZ;
		endcase
	end
	endcase
end
endmodule
