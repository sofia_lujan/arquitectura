`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:23:00 02/25/2014 
// Design Name: 
// Module Name:    Sync_IF_D 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sync_IF_D(

 	input [31:0]Banco_registro_cod,
	input [31:0]Sumador_fetch,
	input clk,
	output reg [31:0]Banco_registro_cod_Sync_IF_D,
	output reg [31:0]Sumador_fetch_Sync_IF_D
	
);

always @ (posedge clk)
begin
Banco_registro_cod_Sync_IF_D = Banco_registro_cod;
Sumador_fetch_Sync_IF_D = Sumador_fetch;
end

endmodule