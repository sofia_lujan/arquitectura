`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:18:05 04/21/2014
// Design Name:   PC_fetch
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/tp_final_v1/test_PC_fetch.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: PC_fetch
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_PC_fetch;

	// Inputs
	reg [31:0] mux_jump_exe;
	reg clk;

	// Outputs
	wire [31:0] PC_fetch;

	// Instantiate the Unit Under Test (UUT)
	PC_fetch uut (
		.mux_jump_exe(mux_jump_exe), 
		.clk(clk), 
		.PC_fetch(PC_fetch)
	);
	always #5 clk=!clk;
	initial begin
		// Initialize Inputs
		mux_jump_exe = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#10;
      mux_jump_exe = 1;
		#10;
      mux_jump_exe = 2;
		#10;
      mux_jump_exe = 4;
		#10;
      mux_jump_exe = 8;


		  
		// Add stimulus here

	end
      
endmodule

