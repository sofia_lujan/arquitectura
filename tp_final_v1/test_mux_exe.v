`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:59:58 04/22/2014
// Design Name:   mux_exe
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_mux_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mux_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux_exe;

	// Inputs
	reg ALUSrc_ControlUnit;
	reg [31:0] extended_instruction_deco;
	reg [31:0] read_data_2_deco;

	// Outputs
	wire [31:0] mux_exe_out;

	// Instantiate the Unit Under Test (UUT)
	mux_exe uut (
		.ALUSrc_ControlUnit(ALUSrc_ControlUnit), 
		.extended_instruction_deco(extended_instruction_deco), 
		.mux_exe_out(mux_exe_out), 
		.read_data_2_deco(read_data_2_deco)
	);

	initial begin
		// Initialize Inputs
		ALUSrc_ControlUnit = 0;
		extended_instruction_deco = 0;
		read_data_2_deco = 0;

		// Wait 100 ns for global reset to finish
		#100;
      ALUSrc_ControlUnit = 0;
		extended_instruction_deco = 4;
		read_data_2_deco = 2;
		#100;
      ALUSrc_ControlUnit = 1;
		extended_instruction_deco = 4;
		read_data_2_deco = 2;
		#100;
      ALUSrc_ControlUnit = 0;
		extended_instruction_deco = 4;
		read_data_2_deco = 2;
		#100;
      ALUSrc_ControlUnit = 1;
		extended_instruction_deco = 4;
		read_data_2_deco = 2;
		// Add stimulus here

	end
      
endmodule

