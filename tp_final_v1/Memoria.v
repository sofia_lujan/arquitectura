`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:14:58 06/10/2014 
// Design Name: 
// Module Name:    Memoria 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Memoria(
	input clk,
	input WRE, //write/read enable
	input [31:0] address,
	input [31:0] write_data,
	input [1:0] memReg_in, 
	input [31:0] sumador_fetch,
	output [31:0] read_data,
	output [1:0]  memReg_output,
	output [31:0] address_in,
	output [31:0] sumador_fetch_Mem_Sync
	
    );

wire [31:0] read_data_wire;
// Instantiate the module
Data_Memory data_memory (
    .clka(clk), 
    .wea(WRE), 
    .addra(address), //direccion que viene en la entradaa
    .dina(write_data), 
    .douta(read_data_wire)
    );
	 
// Instantiate the module
Sync_Mem_WB sync_mem_wb(
    .clk(clk), 
    .read_data_input(read_data_wire), 
	 .memToReg(memReg_in), //valor que entra a esta etapa y pasa a la siguiente
	 .address_in(address_in),
	 .sum_fetch(sumador_fetch), //este valor viene desde la etapa del fetch
    .read_data_output(read_data),
	 .memToReg_output(memReg_output), //es el valor que le tiene que llegar a la etapa siguiente
	 .address_out_Sync(address_in), //este es el valor que viene de la alau de la etapa de ejecución
	 .sum_fetch_Sync(sumador_fetch_Mem_Sync) //este valor es de la salida del modulo 
    );



endmodule
