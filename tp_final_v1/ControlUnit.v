`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:18:11 04/28/2014 
// Design Name: 
// Module Name:    ControlUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ControlUnit(
    output reg [1:0] jump_ControlUnit,
    output reg[1:0] MemtoReg_ControlUnit,
    output reg[1:0] RegDst_ControlUnit,
    output reg[2:0] ALUOp_ControlUnit,
    output reg ALUSrc_ControlUnit,
    output reg Branch_ControlUnit,
    input [31:0] intruccion_fin_deco,
    output reg MemRead_ControlUnit,
    output reg MemWrite_ControlUnit,
    output reg RegWrite_ControlUnit //esta salida entra al Register File
    );

reg [5:0] instruccion ;

always @*
begin
	instruccion = intruccion_fin_deco [31:26];
	case(instruccion)
		6'b000000:
			begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b01;
				ALUOp_ControlUnit = 3'b111;
				ALUSrc_ControlUnit = 0;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
			end
		6'b001000: //ADDI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b000;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001001: //ADDIU
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b000;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001100: //ANDI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b001;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001101: //ORI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b010;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001110: //ORI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b011;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001111: //LUI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b000;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001010: //SLTI
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b110;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b001011: //SLTIU
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b110;
				ALUSrc_ControlUnit = 1;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b000100: //BEQ
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b100;
				ALUSrc_ControlUnit = 0;
				Branch_ControlUnit = 1;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 0;
		end
		6'b000101: //BNE
		begin
				jump_ControlUnit = 2'b00;
				MemtoReg_ControlUnit = 2'b00;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b100;
				ALUSrc_ControlUnit = 0;
				Branch_ControlUnit = 1;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 0;
		end
		6'b000010: //J
		begin
				jump_ControlUnit = 2'b01;
				MemtoReg_ControlUnit = 2'bxx;
				RegDst_ControlUnit = 2'bxx;
				ALUOp_ControlUnit = 3'bxxx;
				ALUSrc_ControlUnit = 1'bx;
				Branch_ControlUnit = 1'bx;
				MemRead_ControlUnit = 1'bx;
				MemWrite_ControlUnit = 1'bx;
				RegWrite_ControlUnit = 1'bx;
		end
		6'b000011: //JAL
		begin
				jump_ControlUnit = 2'b01;
				MemtoReg_ControlUnit = 2'b10;
				RegDst_ControlUnit = 2'b10;
				ALUOp_ControlUnit = 3'bxxx;
				ALUSrc_ControlUnit = 1'bx;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b000000: //JR
		begin
				jump_ControlUnit = 2'b10;
				MemtoReg_ControlUnit = 2'bxx;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b111;
				ALUSrc_ControlUnit = 0;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 0;
		end
		6'b000000: //JALR
		begin
				jump_ControlUnit = 2'b10;
				MemtoReg_ControlUnit = 2'b01;
				RegDst_ControlUnit = 2'b00;
				ALUOp_ControlUnit = 3'b111;
				ALUSrc_ControlUnit = 0;
				Branch_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 0;
				RegWrite_ControlUnit = 1;
		end
		6'b100000: //LB
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b100001: //LH
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b100011: //LW
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b100111: //LWU
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b100100: //LBU
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b100101: //LHU
		begin
				RegDst_ControlUnit = 2'b00;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b01;
				RegWrite_ControlUnit = 1;
				MemRead_ControlUnit = 1;
				MemWrite_ControlUnit = 0;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b101000: //SB
		begin
				RegDst_ControlUnit = 2'b0x;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b0x;
				RegWrite_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 1;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b101001: //SH
		begin
				RegDst_ControlUnit = 2'b0x;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b0x;
				RegWrite_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 1;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
		6'b101011: //SW
		begin
				RegDst_ControlUnit = 2'b0x;
				ALUSrc_ControlUnit = 1;
				MemtoReg_ControlUnit = 2'b0x;
				RegWrite_ControlUnit = 0;
				MemRead_ControlUnit = 0;
				MemWrite_ControlUnit = 1;
				Branch_ControlUnit = 0;
				ALUOp_ControlUnit = 3'b000;
				jump_ControlUnit = 2'b00;				
		end
	endcase
end

endmodule