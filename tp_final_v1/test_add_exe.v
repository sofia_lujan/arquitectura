`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:51:36 05/27/2014
// Design Name:   add_exe
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_add_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: add_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_add_exe;

	// Inputs
	reg branch_flag;
	reg zero_flag;

	// Outputs
	wire add_exe_output;

	// Instantiate the Unit Under Test (UUT)
	add_exe uut (
		.branch_flag(branch_flag), 
		.zero_flag(zero_flag), 
		.add_exe_output(add_exe_output)
	);

	initial begin
		// Initialize Inputs
		branch_flag = 0;
		zero_flag = 0;

		// Wait 100 ns for global reset to finish
		#100;
		branch_flag = 0;
		zero_flag = 0;
		#100;
		branch_flag = 0;
		zero_flag = 1;
		#100;
		branch_flag = 1;
		zero_flag = 0;
		#100;
		branch_flag = 1;
		zero_flag = 1;		
		// Add stimulus here

	end
      
endmodule

