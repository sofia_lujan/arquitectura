`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:46:16 04/22/2014
// Design Name:   ALU_Control
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_ALU_Control.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALU_Control
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_ALU_Control;

	// Inputs
	reg [2:0] ALUOp_controlUnit;
	reg [5:0] instruccion_deco;

	// Outputs
	wire [3:0] ALU_Control_exe;

	// Instantiate the Unit Under Test (UUT)
	ALU_Control uut (
		.ALU_Control_exe(ALU_Control_exe), 
		.ALUOp_controlUnit(ALUOp_controlUnit), 
		.instruccion_deco(instruccion_deco)
	);

	initial begin
		// Initialize Inputs
		ALUOp_controlUnit = 0;
		instruccion_deco = 0;

		// Wait 100 ns for global reset to finish
		#100;
      ALUOp_controlUnit = 3'b000;
		instruccion_deco = 0;
		#100;
      ALUOp_controlUnit = 3'b001;
		instruccion_deco = 0;
		#100;
      ALUOp_controlUnit = 3'b010;
		instruccion_deco = 0;
		#100;
      ALUOp_controlUnit = 3'b011;
		instruccion_deco = 0;
		#100;
      ALUOp_controlUnit = 3'b111;
		instruccion_deco = 6'b100000;
		// Add stimulus here

	end
      
endmodule

