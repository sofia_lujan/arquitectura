`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:24:13 05/25/2014 
// Design Name: 
// Module Name:    Sync_Exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sync_Exe(
	input clk,
	input [31:0] mux_jump_exe, //esta es la salida del m�dulo mux_jump_exe
	input [31:0] ALU_result_exe, //este valor viene de la alu_exe
	input [31:0] mux_branch_exe_i, //este valor es la salida del muxBranch //ES SOLO PARA CONTROL
	input [1:0]	 memReg, //este valor viene de la etapa anterior
	input [31:0] data2, 
	input [31:0] sumador_fetch, //este valor viene de la etapa desde el fetch y tiene que ir hasta la ultima etapa
	output reg [31:0] mux_jump_exe_Sync_Exe,
	output reg [31:0] ALU_result_exe_Sync_Exe,
	output reg [31:0] mux_branch_exe_i_Sync_Exe, //este valor es la salida del muxBranch //ES SOLO PARA CONTROL
	output reg [1:0]	memReg_Sync_Exe, //este valor pasa a la etapa siguiente
	output reg [31:0] data2_output_Sync_Exe,
	output reg [31:0] sumador_fetch_Sync_Exe 
    );

always @ (posedge clk)
begin
mux_jump_exe_Sync_Exe = mux_jump_exe;
ALU_result_exe_Sync_Exe = ALU_result_exe;
mux_branch_exe_i_Sync_Exe = mux_branch_exe_i; //ESTO ES SOLO PARA CONTROL
memReg_Sync_Exe = memReg;
data2_output_Sync_Exe = data2;
sumador_fetch_Sync_Exe = sumador_fetch;
end

endmodule
