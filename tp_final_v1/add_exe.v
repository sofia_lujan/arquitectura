`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:49:19 05/27/2014 
// Design Name: 
// Module Name:    add_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module add_exe(
	input branch_flag,
	input zero_flag,
	output reg add_exe_output
    );

always @*
begin
add_exe_output = branch_flag & zero_flag;
end
endmodule
