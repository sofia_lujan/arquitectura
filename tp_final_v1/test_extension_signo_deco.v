`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:14:33 04/21/2014
// Design Name:   Extension_signo_deco
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_extension_signo_deco.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Extension_signo_deco
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_extension_signo_deco;

	// Inputs
	reg [15:0] instruccion_deco;

	// Outputs
	wire [31:0] extended_instruction_deco;

	// Instantiate the Unit Under Test (UUT)
	Extension_signo_deco uut (
		.instruccion_deco(instruccion_deco), 
		.extended_instruction_deco(extended_instruction_deco)
	);

	initial begin
		// Initialize Inputs
		instruccion_deco = 0;

		// Wait 100 ns for global reset to finish
		#100;
      instruccion_deco = 16'b1000111100001111;
		//instruccion_deco = 16'b0000000000000000;
		//#50;
		//instruccion_deco = 16'b1111000011110000;
		
		// Add stimulus here

	end
      
endmodule

