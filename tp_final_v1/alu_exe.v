`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:52:10 04/22/2014 
// Design Name: 
// Module Name:    alu_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu_exe(
	input [4:0] shift_amount,
	input [3:0] ALU_Control_exe, //con estos 4 bits nos indican la operaci�n que debemos hacer en la alu
	input [31:0] mux_exe, //datos del registro 2 (del banco de registros)
	input signed [31:0] read_data_1_deco, //datos del registro 1 (viene del banco de registros)
	//output reg overflow_alu_exe, //se indica el overflow, MOMENTANEAMENTE NO SE IMPLEMENTA, PORQUE NO CONECTAMOS DICHO FLAG A NINGUN LADO
	output reg [31:0] ALU_result_exe, //resultado de la alu
	output reg zero_alu_exe //flag del cero
    );

reg [31:0]aux; //esta variable la usamos para las operac

always @*
begin
case (ALU_Control_exe)
4'b0000: begin 
			ALU_result_exe = read_data_1_deco & mux_exe; //operaci�n and signed
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;
			end
			
4'b0001: begin
			ALU_result_exe = read_data_1_deco | mux_exe; //operaci�n or
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;
			end
			
4'b0010: begin
			ALU_result_exe = read_data_1_deco + mux_exe; //operaci�n add (suma con signo)
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;			
			end
			
4'b0011: begin
			ALU_result_exe = read_data_1_deco ^ mux_exe; //operaci�n xor (or exclusivo)
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;
			end
			
4'b0100: begin
			ALU_result_exe = read_data_1_deco >> mux_exe; //operaci�n srl(desplazamiento l�gico, sin signo)
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;
			end
			
4'b0101: begin
			ALU_result_exe = read_data_1_deco >>> mux_exe; //operaci�n sra (desplazamiento aritm�tico, con signo)
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;
			end
			
4'b0110: begin
			ALU_result_exe = read_data_1_deco - mux_exe; //operaci�n sub
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;			
			end
			
//slt compara los dos registros, si el primero es menor, entonces a la salida pone un 1, en caso contrario pone 0
4'b0111: begin   //operaci�n slt
			aux = read_data_1_deco - mux_exe; //hacemos la sustraci�n en una variable aux para no modificar la salida
			if(aux[31]==1)  begin ALU_result_exe =1;  zero_alu_exe = 0; end
			else begin ALU_result_exe=0; zero_alu_exe = 1; end		//si el resultado es cero, tambi�n levantamos esa flag
			end 
			
4'b1000: begin 
			ALU_result_exe = read_data_1_deco << mux_exe; //operaci�n sll (desplazamiento l�gico )
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;	
			end
			
4'b1001: begin
			ALU_result_exe = ~(read_data_1_deco | mux_exe); //operaci�n  nor
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;	
			end
4'b1010: begin
			ALU_result_exe = read_data_1_deco + mux_exe; //operaci�n  addu
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;	
			end
4'b1011: begin
			ALU_result_exe = read_data_1_deco - mux_exe; //operaci�n  subu
			if (ALU_result_exe == 0) zero_alu_exe = 1; //controlamos si el resultado es nulo o no
			else zero_alu_exe = 0;	
			end
4'b1100: begin 
			aux = read_data_1_deco - mux_exe; //hacemos la sustraci�n en una variable aux para no modificar la salida
			if(aux[31]==1)  begin ALU_result_exe =1;  zero_alu_exe = 0; end
			else begin ALU_result_exe=0; zero_alu_exe = 1; end		//si el resultado es cero, tambi�n levantamos esa flag
			end 
			
//hacemos que si cae en alguno de los casos que no deber�a darse ponemos todo en alta impedancia
4'b1101: begin 
			ALU_result_exe = 32'bz;
			zero_alu_exe = 1'bz;
			end
4'b1110: begin 
			ALU_result_exe = 32'bz;
			zero_alu_exe = 1'bz;
			end
4'b1111: begin 
			ALU_result_exe = 32'bz;
			zero_alu_exe = 1'bz;
			end
endcase
end

endmodule
