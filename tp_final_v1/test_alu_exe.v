`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:48:03 04/22/2014
// Design Name:   alu_exe
// Module Name:   C:/Users/Belen/Desktop/arquitectura/tp_final_v1/test_alu_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: alu_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_alu_exe;

	// Inputs
	reg [3:0] ALU_Control_exe;
	reg [31:0] mux_exe;
	reg [31:0] read_data_1_deco;

	// Outputs
	//wire overflow_alu_exe;
	wire [31:0] ALU_result_exe;
	wire zero_alu_exe;

	// Instantiate the Unit Under Test (UUT)
	alu_exe uut (
		.ALU_Control_exe(ALU_Control_exe), 
		.mux_exe(mux_exe), 
		.read_data_1_deco(read_data_1_deco), 
		//.overflow_alu_exe(overflow_alu_exe), 
		.ALU_result_exe(ALU_result_exe), 
		.zero_alu_exe(zero_alu_exe)
	);

	initial begin
		// Initialize Inputs
		ALU_Control_exe = 0;
		mux_exe = 0;
		read_data_1_deco = 0;

		// Wait 100 ns for global reset to finish
		#100;
		mux_exe = 32'h00000004;
		read_data_1_deco =32'h00000051;
      #30;  
		ALU_Control_exe = 4'b0000;
      #30;  
		ALU_Control_exe = 4'b0001;
      #30;  
		ALU_Control_exe = 4'b0010;
		#30;  
		ALU_Control_exe = 4'b0011;
		#30;  
		ALU_Control_exe = 4'b0100;
		#30;  
		ALU_Control_exe = 4'b0101;
		#30;  
		ALU_Control_exe = 4'b0110;
		#30;  
		ALU_Control_exe = 4'b0111;
		#30;  
		ALU_Control_exe = 4'b1000;
		#30;  
		ALU_Control_exe = 4'b1001;
		#30;  
		ALU_Control_exe = 4'b1010;
		#30;  
		ALU_Control_exe = 4'b1011;
		#30;  
		ALU_Control_exe = 4'b1100;
		#30;  
		ALU_Control_exe = 4'b1101;
		#30;  
		ALU_Control_exe = 4'b1110;
		#30;  
		ALU_Control_exe = 4'b1111;
		
		// Add stimulus here

	end
      
endmodule

