`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:15:45 04/21/2014
// Design Name:   shift_register_exe
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_shift_register_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: shift_register_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_shift_register_exe;

	// Inputs
	reg [31:0] extended_instruction_deco;

	// Outputs
	wire [31:0] shift_register_exe;

	// Instantiate the Unit Under Test (UUT)
	shift_register_exe uut (
		.extended_instruction_deco(extended_instruction_deco), 
		.shift_register_exe(shift_register_exe)
	);

	initial begin
		// Initialize Inputs
		extended_instruction_deco = 1;

		// Wait 100 ns for global reset to finish
		#100;
      extended_instruction_deco = 2;
		
		#100;
      extended_instruction_deco = 4;
		// Add stimulus here

	end
      
endmodule

