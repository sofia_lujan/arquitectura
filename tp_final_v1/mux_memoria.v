`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:41:00 04/28/2014 
// Design Name: 
// Module Name:    mux_memoria 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_memoria(
    input [1:0] Memtoreg_ControlUnit,
    input [31:0] ALU_result_exe,
    output reg [31:0] mux_memoria,
    input [31:0] Sumador_fetch,
    input [31:0] DataMemory_Memoria
    );

always @*
begin
case (Memtoreg_ControlUnit)
3'b00: mux_memoria = ALU_result_exe;
3'b01: mux_memoria = DataMemory_Memoria;
3'b10: mux_memoria = Sumador_fetch;
3'b11: mux_memoria = DataMemory_Memoria; //Default ver si es correcto dejar este predeterminado
endcase
end
endmodule
