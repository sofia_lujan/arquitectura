`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:54:40 04/21/2014
// Design Name:   Mem_instruccion_fetch
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/tp_final_v1/test_Mem_instruccion_fetch.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Mem_instruccion_fetch
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_Mem_instruccion_fetch;

	// Inputs
	reg [31:0] PC_fetch;

	// Outputs
	wire [31:0] Banco_registro_cod;

	// Instantiate the Unit Under Test (UUT)
	Mem_instruccion_fetch uut (
		.PC_fetch(PC_fetch), 
		.Banco_registro_cod(Banco_registro_cod)
	);

	initial begin
		// Initialize Inputs
		PC_fetch = 0;

		// Wait 100 ns for global reset to finish
		#10;
      PC_fetch = 4;
		#10;
      PC_fetch = 8;
		#10;
      PC_fetch = 12;
		#10;
      PC_fetch = 16;
		
		// Add stimulus here

	end
      
endmodule

