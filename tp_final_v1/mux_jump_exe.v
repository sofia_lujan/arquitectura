`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:08:39 04/22/2014 
// Design Name: 
// Module Name:    mux_jump_exe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_jump_exe(
    input  [1:0] jump_ControlUnit,
	 input [31:0] mux_branch_exe,
	 input [31:0] shift_register_jump,
	 input [31:0] ALU_result_exe,
	 //output reg [31:0] mux_jump_exe
	 output [31:0] mux_jump_exe
    );

/*always @ (jump_ControlUnit)
begin
case (jump_ControlUnit)
2'b00: mux_jump_exe = mux_branch_exe; 
2'b01: mux_jump_exe = shift_register_jump; 
2'b10: mux_jump_exe = ALU_result_exe; 
//default: mux_jump_exe = ALU_result_exe;
2'b11: mux_jump_exe = mux_branch_exe;
endcase
end 
*/

assign mux_jump_exe = (jump_ControlUnit == 2'b00) ? mux_branch_exe :
           (jump_ControlUnit == 2'b01) ? shift_register_jump :
           (jump_ControlUnit == 2'b10) ? ALU_result_exe :
           (jump_ControlUnit == 2'b11) ? mux_branch_exe:mux_branch_exe;






endmodule
