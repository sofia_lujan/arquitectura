`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:58:43 04/22/2014
// Design Name:   shift_register_jump
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_shift_register_jump.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: shift_register_jump
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_shift_register_jump;

	// Inputs
	reg [25:0] instruccion;

	// Outputs
	wire [27:0] shift_register_jump;

	// Instantiate the Unit Under Test (UUT)
	shift_register_jump uut (
		.instruccion(instruccion), 
		.shift_register_jump(shift_register_jump)
	);

	initial begin
		// Initialize Inputs
		instruccion = 0;

		// Wait 100 ns for global reset to finish
		#100;
       instruccion = 26'b11111111111111111111111111; 
		 #115
		 instruccion = 26'b11111111111111111111100001; 
		 
		// Add stimulus here

	end
      
endmodule

