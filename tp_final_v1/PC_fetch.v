`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:21:46 02/25/2014 
// Design Name: 
// Module Name:    PC_fetch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module PC_fetch(

 	input [31:0] mux_jump_exe, //valor que viene de la etapa de ejecuci�n luego de ejecutar un salto
	input clk,
	output reg [31:0] PC_fetch //valor de la pr�xim posici�n de memoria (se env�a al Instruction Memory y al Sumador)
);

always @ (posedge clk)
begin
PC_fetch = mux_jump_exe; //toma el valor de entrada y lo pone a la salida
end

endmodule
