`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:22:11 02/25/2014 
// Design Name: 
// Module Name:    Sumador_fetch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sumador_fetch(
	input [31:0] PC_fetch,  //valor que viene del m�dulo PC al que luego se lo incrementa en 4Bytes
	output reg [31:0] Sumador_fetch  //valor de la pr�xima posici�n de memoria (ya incrementada)
);



always @*
begin
Sumador_fetch = PC_fetch + 3'b100;  //calculamos la pr�xima posici�n de memoria a leer
end
endmodule
