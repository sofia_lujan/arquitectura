`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:41:10 04/21/2014
// Design Name:   sumador_exe
// Module Name:   C:/Users/SofiL/Desktop/arquitectura/New folder/tp_final_v1/test_sumador_exe.v
// Project Name:  tp_final_v1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sumador_exe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_sumador_exe;

	// Inputs
	reg [31:0] shift_register_exe;
	reg [31:0] Sumador_fetch;

	// Outputs
	wire [31:0] sumador_exe;

	// Instantiate the Unit Under Test (UUT)
	sumador_exe uut (
		.shift_register_exe(shift_register_exe), 
		.Sumador_fetch(Sumador_fetch), 
		.sumador_exe(sumador_exe)
	);

	initial begin
		// Initialize Inputs
		shift_register_exe = 0;
		Sumador_fetch = 0;

		// Wait 100 ns for global reset to finish
		#100;
      shift_register_exe = 32'h00000008;
		Sumador_fetch = 32'h00000008;  
		// Add stimulus here

	end
      
endmodule

